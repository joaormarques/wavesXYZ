# Waves XYZ
This project pretends to create virtual environments that react to sound.
The sound can be either PRODUCED live (by a mic or line in) or REPRODUCED (using a audio file)
I use A-frame to create virtual environment that can be visualized either in a monitor or a cardboard or any goggles supported by a-frame
I use Angular.js because it's easier to control the DOM
I use Web Audio API to access the device microphone or input or music file.

Theres a lot to do but meanwhile... here's some pictures:

![Bass string](public/img/final/bass-string.jpg)
![Sphere](public/img/final/sphere.jpg)
![Bubbles](public/img/final/bubbles.jpg)
![Cylinder](public/img/final/cylinder.jpg)
![Vortex](public/img/final/vortex.jpg)
![Spiral](public/img/final/spiral.jpg)