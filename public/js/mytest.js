

var audioContext = null;
var rafID = null;
var myData = {};
var analyser;
var frequencyData;

function loadDogSound(success, error) {
  var request = new XMLHttpRequest();
  request.open('GET', "../../audio/moanin.mp3", true);
  request.responseType = 'arraybuffer';

  // Decode asynchronously
  request.onload = function() {
    audioContext.decodeAudioData(request.response, function(buffer) {
      success(buffer)
    }, error);
  }
  request.send();
}

window.onload = function() {
    var musicOrStream = true;
    // monkeypatch Web Audio
    window.AudioContext = window.AudioContext || window.webkitAudioContext;

    // grab an audio context
    audioContext = new AudioContext();

    // Attempt to get audio input
    try {
        // monkeypatch getUserMedia
        navigator.getUserMedia =
            navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia;


        if(musicOrStream){
            //ask for an audio input
            navigator.getUserMedia(
            {
                "audio": {
                    "mandatory": {
                        "googEchoCancellation": "false",
                        "googAutoGainControl": "false",
                        "googNoiseSuppression": "false",
                        "googHighpassFilter": "false"
                    },
                    "optional": []
                },
            }, gotStream, didntGetStream);
        }else{
            loadDogSound(gotBuffer, didntGetStream);
        }
    } catch (e) {
        alert('getUserMedia threw exception :' + e);
    }
}
function didntGetStream() {
    alert('Stream generation failed.');
}



var mediaStreamSource = null;

function gotBuffer(buffer) {
    mediaStreamSource = audioContext.createBufferSource();
    mediaStreamSource.buffer = buffer;

    analyser = audioContext.createAnalyser();
    analyser.fftSize = 512; //can be 128 256 512 1024 2048
    analyser.smoothingTimeConstant = 0;
    mediaStreamSource.connect(analyser);
    analyser.connect(audioContext.destination);
    //console.log(analyser.frequencyBinCount); // fftSize/2 = 32 data points
    frequencyData = new Uint8Array(analyser.frequencyBinCount);

    mediaStreamSource.start(0);

    update();
}

function gotStream(stream) {
    // Create an AudioNode from the stream.
    mediaStreamSource = audioContext.createMediaStreamSource(stream);


    //create an analyser
    analyser = audioContext.createAnalyser();
    analyser.fftSize = 1024;
    mediaStreamSource.connect(analyser);
    analyser.connect(audioContext.destination);
    //console.log(analyser.frequencyBinCount); // fftSize/2 = 32 data points
    frequencyData = new Uint8Array(analyser.frequencyBinCount);

    // kick off the visual updating
    update();
}

function update( time ) {

    analyser.getByteFrequencyData(frequencyData);

    if(window.updateFanfas) {
        window.updateFanfas(frequencyData);
    }
   // updateRadius($(frequencyData).filter(function(n,i) { return i > 0;}).length * 0.2);
    //console.log(frequencyData);
    //console.log(time);
    // set up the next visual callback
    rafID = window.requestAnimationFrame( update );
}